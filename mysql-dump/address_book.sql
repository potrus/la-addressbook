-- MySQL Script generated by MySQL Workbench
-- Thu 19 Dec 2019 05:09:41 AM CET
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema lumenad_address_book
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema lumenad_address_book
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lumenad_address_book` DEFAULT CHARACTER SET utf8 ;
USE `lumenad_address_book` ;

-- -----------------------------------------------------
-- Table `lumenad_address_book`.`la_contact`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lumenad_address_book`.`la_contact` ;

CREATE TABLE IF NOT EXISTS `lumenad_address_book`.`la_contact` (
  `contact_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(64) NOT NULL,
  `last_name` VARCHAR(128) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_id`),
  UNIQUE INDEX `contact_id_UNIQUE` (`contact_id` ASC))
ENGINE = InnoDB
COMMENT = 'Adress book with contacts';


-- -----------------------------------------------------
-- Table `lumenad_address_book`.`la_address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lumenad_address_book`.`la_address` ;

CREATE TABLE IF NOT EXISTS `lumenad_address_book`.`la_address` (
  `address_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(255) NULL,
  `city` VARCHAR(128) NULL,
  `region` VARCHAR(128) NULL,
  `zip_code` VARCHAR(12) NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE INDEX `address_id_UNIQUE` (`address_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lumenad_address_book`.`la_contact_email`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lumenad_address_book`.`la_contact_email` ;

CREATE TABLE IF NOT EXISTS `lumenad_address_book`.`la_contact_email` (
  `contact_email_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_id` BIGINT UNSIGNED NOT NULL,
  `email` VARCHAR(128) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_email_id`),
  UNIQUE INDEX `contact_email_id_UNIQUE` (`contact_email_id` ASC),
  CONSTRAINT `fk_la_contact_email_contactid`
    FOREIGN KEY (`contact_id`)
    REFERENCES `lumenad_address_book`.`la_contact` (`contact_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lumenad_address_book`.`la_contact_phone_number`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lumenad_address_book`.`la_contact_phone_number` ;

CREATE TABLE IF NOT EXISTS `lumenad_address_book`.`la_contact_phone_number` (
  `contact_phone_number_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_id` BIGINT UNSIGNED NOT NULL,
  `phone_number` VARCHAR(16) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_phone_number_id`),
  UNIQUE INDEX `contact_phone_number_id_UNIQUE` (`contact_phone_number_id` ASC),
  CONSTRAINT `fk_contact_phone_number_contactid`
    FOREIGN KEY (`contact_id`)
    REFERENCES `lumenad_address_book`.`la_contact` (`contact_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lumenad_address_book`.`la_contact_address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lumenad_address_book`.`la_contact_address` ;

CREATE TABLE IF NOT EXISTS `lumenad_address_book`.`la_contact_address` (
  `contact_address_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_id` BIGINT UNSIGNED NOT NULL,
  `address_id` BIGINT UNSIGNED NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `fk_la_contact_address_idx` (`address_id` ASC),
  PRIMARY KEY (`contact_address_id`),
  UNIQUE INDEX `contact_address_id_UNIQUE` (`contact_address_id` ASC),
  CONSTRAINT `fk_contact_address_contactid`
    FOREIGN KEY (`contact_id`)
    REFERENCES `lumenad_address_book`.`la_contact` (`contact_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_contact_address_addressid`
    FOREIGN KEY (`address_id`)
    REFERENCES `lumenad_address_book`.`la_address` (`address_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
