# Address Book

Single page application in react.js with ability:

+ to add new contacts
+ to edit contacts
+ to delete contacts
+ to View all Contacts by name in a list
+ to view a single contact with information

Missing (should be done first):

+ authentication to REST API
+ propagation of REST validation errors
+ support for multiple emails, phone numbers and addresses (database is ready but functionality is not supported in GUI)

Could be nice:

+ pagination 
+ filter input for fast search contacts

## Running in Docker

## Prerequisites
Docker 1.13.0 or higher and docker-compose 1.10.0 or higher.

### Installation

Inside the project directory, run:
```
$ docker-compose up -d
```
When docker is running, do:
```
docker exec lumenad-laravel composer install
docker exec lumenad-laravel chown -R www-data:www-data /var/www/laravel
docker exec lumenad-laravel chmod -R 755 /var/www/laravel/storage
``` 

Inside of git repository, rename ```.env.example``` to ```.env```:
```
mv .env.example .env
```