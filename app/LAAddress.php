<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LAAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'la_address';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'address_id';

    /**
     * Disabled created_at and updated_at for associated model
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * Mass assignable fields associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'address',
        'city',
        'region',
        'zip_code',
    ];


    /**
     * Get the contact records associated with the address
     */
    public function contacts()
    {
        return $this->hasMany('App\LAContactAddress', 'address_id');
    }
}
