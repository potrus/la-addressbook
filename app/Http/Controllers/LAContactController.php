<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class LAContactController extends Controller
{
    /**
     * Get all contacts with additional data
     *
     * @param  \App\LAContact   $dbhContact
     * @return \Illuminate\Http\Response
     */
    public function index (\App\LAContact $dbhContact) {
        $contacts = $dbhContact->all();
        $result = [];

        foreach ($contacts as $c) {
            $result[] = $this->fetchContactFullData($c->contact_id, $c);
        }

        return json_encode($result, true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request   $request
     * @param  \App\LAContact             $dbhContact
     * @param  \App\LAAddress             $dbhAddress
     * @param  \App\LAContactEmail        $dbhContactEmail,
     * @param  \App\LAContactPhoneNumber  $dbhContactPhoneNumber,
     * @param  \App\LAContactAddress      $dbhContactAddress
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        \App\LAContact      $dbhContact,
        \App\LAAddress      $dbhAddress,
        \App\LAContactEmail        $dbhContactEmail,
        \App\LAContactPhoneNumber  $dbhContactPhoneNumber,
        \App\LAContactAddress      $dbhContactAddress
    ) {
        # Request data validation
        $validator = Validator::make($request->all(), [
            'first_name'   => ['required', 'string:64'],
            'last_name'    => ['required', 'string:128'],
            'email'        => ['email'],
            'phone_number' => ['regex:/^[0-9]{9,12}$/'],     # very simple validation
            'address'      => ['string:255'],
            'city'         => ['string:128'],
            'region'       => ['string:128'],
            'zip_code'     => ['string:12'],
        ]);

        if ($validator->fails()) {
            return response()->json('Unprocessable Entity', 422);
        }
        $validatedData = $validator->validated();

        # Store contact
        $contact = $dbhContact->create([
            'first_name' => $validatedData['first_name'],
            'last_name'  => $validatedData['last_name'],
        ]);

        # Store email
        if ( array_key_exists('email', $validatedData) ) {
            $dbhContactEmail->create([
                'contact_id' => $contact->contact_id,
                'email'      => $validatedData['email'],
            ]);
        }

        # Store phone number
        if ( array_key_exists('phone_number', $validatedData) ) {
            $dbhContactPhoneNumber->create([
                'contact_id'   => $contact->contact_id,
                'phone_number' => $validatedData['phone_number'],
            ]);
        }

        # Store address
        $addressData = [];

        foreach ( ['address', 'city', 'region', 'zip_code'] as $key ) {
            if ( array_key_exists($key, $validatedData) ) {
                $addressData[$key] = $validatedData[$key];
            }
        }

        $address = null;
        if ( $addressData ) {
            $address = $dbhAddress->create($addressData);
        }

        # Store connection between contact and address
        if ( $address ) {
            $dbhContactAddress->create([
                'contact_id' => $contact->contact_id,
                'address_id' => $address->address_id,
            ]);
        }

        return response()->json($contact, 201);
    }

    /*
     * Get contact with supplied ID
     *
     * @param  String           $contactId
     * @param  \App\LAContact   $dbhContact
     * @return \Illuminate\Http\Response
     */
    public function show (String $contactId, \App\LAContact $dbhContact) {
        $result = $this->fetchContactFullData($contactId, $dbhContact);

        if (!$result) {
            return response()->json('Not Found', 404);
        }

        return json_encode($result, true);
    }

    /**
     * Retrive full data for contact
     *
     * @param  String          $contactId
     * @param  \App\LAContact  $dbhContact
     * @return Array
     */
    private function fetchContactFullData($contactId, $dbhContact) {
        $contact = $dbhContact->find($contactId);

        if (!$contact) {
            return null;
        }

        $phoneNumber = $contact->phoneNumbers()->first();
        $email       = $contact->emails()->first();
        $address     = $contact->addresses()->first();

        $result = $contact->toArray();

        if ($phoneNumber) {
            $result['contact_phone_number_id'] = $phoneNumber->contact_phone_number_id;
            $result['phone_number']            = $phoneNumber->phone_number;
        }

        if ($email) {
            $result['contact_email_id'] = $email->contact_email_id;
            $result['email']            = $email->email;
        }

        if ($address) {
            $result = array_merge($result, $address->address()->first()->toArray());
        }

        return $result;
    }

    /**
     * Update contact by contact ID
     *
     * @param  String                     $contactId
     * @param  \Illuminate\Http\Request   $request
     * @param  \App\LAContact             $dbhContact
     * @param  \App\LAAddress             $dbhAddress
     * @param  \App\LAContactEmail        $dbhContactEmail,
     * @param  \App\LAContactPhoneNumber  $dbhContactPhoneNumber,
     * @param  \App\LAContactAddress      $dbhContactAddress
     *
     * @return \Illuminate\Http\Response
     */
    public function update(
        String $contactId,
        Request $request,
        \App\LAContact             $dbhContact,
        \App\LAAddress             $dbhAddress,
        \App\LAContactEmail        $dbhContactEmail,
        \App\LAContactPhoneNumber  $dbhContactPhoneNumber,
        \App\LAContactAddress      $dbhContactAddress
    ) {
        # Request data validation
        $validator = Validator::make($request->all(), [
            'first_name'   => ['required', 'string:64'],
            'last_name'    => ['required', 'string:128'],
            'email'        => ['email'],
            'phone_number' => ['regex:/^[0-9]{9,12}$/'],     # very simple validation
            'address'      => ['string:255'],
            'city'         => ['string:128'],
            'region'       => ['string:128'],
            'zip_code'     => ['string:12'],
            'contact_email_id'        => ['integer'],
            'contact_phone_number_id' => ['integer'],
            'contact_address_id'      => ['integer'],
        ]);

        if ($validator->fails()) {
            return response()->json('Unprocessable Entity', 422);
        }
        $validatedData = $validator->validated();

        # Update contact
        $contact = $dbhContact->find($contactId);

        if (!$contact) {
            return response()->json('Not Found', 404);
        }

        $contact->save([
            'first_name' => $validatedData['first_name'],
            'last_name'  => $validatedData['last_name'],
        ]);

        # Update or create mail
        if ( array_key_exists('email', $validatedData) ) {
            $dbhContactEmail->updateOrCreate(
                [
                    'contact_email_id' => ( array_key_exists('contact_email_id', $validatedData)
                        ? $validatedData['contact_email_id']
                        : null
                    ),
                ],
                [
                    'contact_id' => $contact->contact_id,
                    'email'      => $validatedData['email'],
            ]);
        }

        # Update or create phone number
        if ( array_key_exists('phone_number', $validatedData) ) {
            $dbhContactPhoneNumber->updateOrCreate(
                [
                    'contact_phone_number_id' => ( array_key_exists('contact_phone_number_id', $validatedData)
                        ? $validatedData['contact_phone_number_id']
                        : null
                    ),
                ],
                [
                    'contact_id'   => $contact->contact_id,
                    'phone_number' => $validatedData['phone_number'],
            ]);
        }

        # Update address
        $addressData = [];

        foreach ( ['address', 'city', 'region', 'zip_code'] as $key ) {
            $addressData[$key] = array_key_exists($key, $validatedData)
                ? $validatedData[$key]
                : '';
        }

        if ( array_key_exists('contact_address_id', $validatedData) ) {
            $contactAddress = $dbhContactAddress->find($validatedData['contact_address_id']);
            $addressData['address_id'] = $contactAddress->address_id;
        }

        $address = null;
        if ($addressData) {
            $address = $dbhAddress->updateOrCreate(
                [
                    'address_id' => ( array_key_exists('address_id', $addressData)
                        ? $addressData['address_id']
                        : null
                    ),
                ],
                $addressData
            );
        }

        # Store connection between contact and address
        if ( $address ) {
            $dbhContactAddress->updateOrCreate([
                'contact_address_id' => ( array_key_exists('contact_address_id', $validatedData)
                    ? $validatedData['contact_address_id']
                    : null
                ),
                'contact_id' => $contact->contact_id,
                'address_id' => $address->address_id,
            ]);
        }

        return response()->json($contact, 200);
    }

    /**
     * Delete contact by contact ID
     *
     * @param  String                     $contactId
     * @param  \App\LAContact             $dbhContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        String $contactId,
        \App\LAContact $dbhContact
    ) {
        $contact = $dbhContact->find($contactId);

        if (!$contact) {
            return response()->json('Not Found', 404);
        }

        $dbhContact->destroy($contactId);

        return response()->json(null, 204);
    }
}
