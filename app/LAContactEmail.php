<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LAContactEmail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'la_contact_email';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'contact_email_id';

    /**
     * Disabled created_at and updated_at for associated model
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * Mass assignable fields associated with the model.
     *
     * @var string
     */
    protected $fillable = ['contact_id', 'email'];


    /**
     * Get the contact that owns the e-mail.
     */
    public function contact()
    {
        return $this->belongsTo('App\LAContact', 'contact_id', 'contact_id');
    }
}

