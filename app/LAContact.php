<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LAContact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'la_contact';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'contact_id';

    /**
     * Mass assignable fields associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'first_name',
        'last_name',
    ];

    /**
     * Disabled created_at and updated_at for associated model
     *
     * @var string
     */
    public $timestamps = false;


    /**
     * Get the phone number associated with the contact.
     */
    public function phoneNumbers()
    {
        return $this->hasMany('App\LAContactPhoneNumber', 'contact_id', 'contact_id');
    }

    /**
     * Get the email associated with the contact.
     */
    public function emails()
    {
        return $this->hasMany('App\LAContactEmail', 'contact_id', 'contact_id');
    }

    /**
     * Get the address record associated with the contact.
     */
    public function addresses()
    {
        return $this->hasMany('App\LAContactAddress', 'contact_id', 'contact_id');
    }


}

