<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LAContactAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'la_contact_address';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'contact_address_id';

    /**
     * Disabled created_at and updated_at for associated model
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * Mass assignable fields associated with the model.
     *
     * @var string
     */
    protected $fillable = ['contact_id', 'address_id'];


    /**
     * Get the contact that owns the address
     */
    public function contact()
    {
        return $this->belongsTo('App\LAContact', 'contact_id', 'contact_id');
    }

    /**
     * Get the address record associated with the contact
     */
    public function address()
    {
        return $this->belongsTo('App\LAAddress', 'address_id', 'address_id');
    }
}
