<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LAContactPhoneNumber extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'la_contact_phone_number';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'contact_phone_number_id';

    /**
     * Disabled created_at and updated_at for associated model
     *
     * @var string
     */
    public $timestamps = false;

    /**
     * Mass assignable fields associated with the model.
     *
     * @var string
     */
    protected $fillable = ['contact_id', 'phone_number'];


    /**
     * Get the contact that owns the phone number.
     */
    public function contact()
    {
        return $this->belongsTo('App\LAContact', 'contact_id', 'contact_id');
    }
}
