import React from 'react';

import Panel from './layout/Panel';
import ContactList from './ContactList';
import ContactForm from './ContactForm';
import DeleteConfirmation from './DeleteConfirmation';


export default class AddressBookController extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            view: 'contact-list',
            contact_id: null,
        };

        this.showContactList   = this.showContactList.bind(this);
        this.showContactDetail = this.showContactDetail.bind(this);
        this.showContactAdd    = this.showContactAdd.bind(this);
        this.showContactEdit   = this.showContactEdit.bind(this);
        this.showContactDelete = this.showContactDelete.bind(this);

        this.handleCreateContact = this.handleCreateContact.bind(this);
        this.handleUpdateContact = this.handleUpdateContact.bind(this);
        this.handleDeleteContact = this.handleDeleteContact.bind(this);
    }

    showContactList() {
        this.setState({view: 'contact-list'});
    }

    showContactDetail(contact_id) {
        this.setState({
            view: 'show-contact',
            contact_id: contact_id
        });
    }

    showContactAdd() {
        this.setState({view: 'add-contact'});
    }

    showContactEdit(contact_id) {
        this.setState({
            view: 'edit-contact',
            contact_id: contact_id
        });
    }

    showContactDelete(contact_id) {
        this.setState({
            view: 'delete-contact',
            contact_id: contact_id
        });
    }

    handleCreateContact(contact) {
        const contactFiltered = Object.entries(contact).reduce((a,[k,v]) => (v ? {...a, [k]:v} : a), {});

        fetch( 'http://localhost:8080/api/contact', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(contactFiltered)
        })
        .then(function(response) {
            if (response.ok) {
                window.location.href = 'http://localhost:8080';
            }
        })
        .then(function(jsonData) {
            return JSON.stringify(jsonData);
        })
        .then(function(jsonStr) {
            console.log(jsonStr);
        });
    }

    handleUpdateContact(contact) {
        const contactFiltered = Object.entries(contact).reduce((a,[k,v]) => (v ? {...a, [k]:v} : a), {});

        fetch( 'http://localhost:8080/api/contact/' + contactFiltered.contact_id, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(contactFiltered)
        })
        .then(function(response) {
            if (response.ok) {
                window.location.href = 'http://localhost:8080';
            }
            else {
                console.log(response.codeText);
            }
        });
    }

    handleDeleteContact(contact_id) {
        fetch( 'http://localhost:8080/api/contact/' + contact_id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        });

        this.setState({
            view: 'contact-list',
            contact_id: null
        });
    }

    defineContent() {
        switch(this.state.view) {
        case 'add-contact':
            return <ContactForm
                contact_id    = {null}
                handleSubmit = {this.handleCreateContact}
                handleCancel = {this.showContactList}
            />;
        case 'show-contact':
            return <ContactForm
                contact_id    = {this.state.contact_id}
                handleSubmit = {null}
                handleCancel = {this.showContactList}
            />;
        case 'edit-contact':
            return <ContactForm
                contact_id    = {this.state.contact_id}
                handleSubmit = {this.handleUpdateContact}
                handleCancel = {this.showContactList}
            />;
        case 'delete-contact':
            return <DeleteConfirmation
                contact_id    = {this.state.contact_id}
                handleSubmit = {this.handleDeleteContact}
                handleCancel = {this.showContactList}
            />
        default:
            return <ContactList
                showContactDetail = {this.showContactDetail}
                showContactEdit   = {this.showContactEdit}
                showContactDelete = {this.showContactDelete}
            />;
        }

    }

    addressBookBody() {
        const body = this.defineContent();

        return (
            <div>
                {this.state.view === 'contact-list' &&
                    <button className="btn btn-primary btn-sm" onClick={this.showContactAdd}>
                        <i className="fas fa-plus"></i> Add new contact
                    </button>
                }
                {body}
            </div>
        );
    }

    render() {
        return (
            <Panel
                title = 'Address Book'
                panelClass = 'panel first w-60'
                body = {this.addressBookBody()}
            />
        );
    }
}
