import React from 'react';
import ContactListRow from './ContactListRow';

export default class ContactList extends React.Component {
    constructor (props) {
        super(props);

        this.state = {contacts: []}
    }

    componentDidMount() {
        fetch( 'http://localhost:8080/api/contact')
            .then( res => res.json() )
            .then( (contacts) => this.setState({contacts}) );
    }

    render() {
        let addressList = this.state.contacts.map(
            (contactData) => <ContactListRow
                key={contactData.contact_id}
                contact={contactData}
                showContactDetail = {this.props.showContactDetail}
                showContactEdit   = {this.props.showContactEdit}
                showContactDelete = {this.props.showContactDelete}
            />);

        return (
            <table className="table table-striped table-hover table-sm">
                <caption>List of contacts</caption>
                <thead className="thead-light">
                    <tr>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">E-Mail</th>
                        <th scope="col">Phone</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {addressList}
                </tbody>
            </table>
        );
    }
}
