import React from 'react';

export default class DeleteConfirmation extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            contact_id:   '',
            first_name:   '',
            last_name:    '',
        };

        this.handleSubmitLocal = this.handleSubmitLocal.bind(this);
    }

    handleSubmitLocal(event) {
        this.props.handleSubmit(this.props.contact_id);
    }

    render() {
        const contactId = this.state.contact_id;
        return (
            <form onSubmit={this.handleSubmitLocal}>
                <div className="form-group">
                    Would You really like to delete contact {this.state.first_name} {this.state.last_name}?
                </div>
                <div className="form-row">
                    {this.props.handleSubmit &&
                        <div className="col-xs-1">
                            <button type="submit" className="btn btn-primary">Delete</button>
                        </div>
                    }
                    <div className="col-xs-1">
                        <button type="button" className="btn btn-secondary" onClick={this.props.handleCancel}>Cancel</button>
                    </div>
                </div>
            </form>
        );
    }
}
