import React from 'react';

export default class ContactListRow extends React.Component {
    constructor (props)  {
        super(props);

        this.showDetails.bind(this);
        this.editContact.bind(this);
        this.deleteContact.bind(this);
    }

    showDetails(contact_id) {
        this.props.showContactDetail(contact_id);
    }

    editContact(contact_id) {
        this.props.showContactEdit(contact_id);
    }

    deleteContact(contact_id) {
        this.props.showContactDelete(contact_id);
    }

    render() {
        return (
            <tr>
                <td className="cell">{this.props.contact.first_name}</td>
                <td className="cell">{this.props.contact.last_name}</td>
                <td className="cell">{this.props.contact.email}</td>
                <td className="cell">{this.props.contact.phone_number}</td>
                <td className="cell center">
                    <button className="btn btn-link" onClick={this.showDetails.bind(this, this.props.contact.contact_id)}>
                        <i className="fas fa-info" title="Show contact deails"></i>
                    </button>
                    <button className="btn btn-link" onClick={this.editContact.bind(this, this.props.contact.contact_id)}>
                        <i className="fas fa-edit" title="Edit contact"></i>
                    </button>
                    <button className="btn btn-link" onClick={this.deleteContact.bind(this, this.props.contact.contact_id)}>
                        <i className="fas fa-trash-alt" title="Remove contact"></i>
                    </button>
                </td>
            </tr>
        );
    }
}
