import React from 'react';

export default class ContactForm extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            contact_id:   '',
            first_name:   '',
            last_name:    '',
            email:        '',
            phone_number: '',
            address_id:   '',
            address:      '',
            city:         '',
            region:       '',
            zip_code:     '',
            contact_email_id: '',
            contact_phone_number_id: '',
            contact_address_id: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmitLocal = this.handleSubmitLocal.bind(this);
    }

    componentDidMount() {
        if ( !this.props.contact_id ) {
            return;
        }

        fetch( 'http://localhost:8080/api/contact/' + this.props.contact_id)
            .then( res => res.json() )
            .then( (contact) => {
                for ( let key in contact ) {
                    this.setState({ [key] : contact[key] });
                };
            });
    }

    handleInputChange(event) {
        const target = event.target;
        const name   = target.name;
        const value  = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({
            [name]: value
        });
    }

    handleSubmitLocal(event) {
        event.preventDefault();
        this.props.handleSubmit(this.state);
    }

    render() {
        let formTitle, submitName;

        if ( !this.props.contact_id ) {
            formTitle  = 'Add new contact';
            submitName = 'Add contact';
        }
        else if ( this.props.handleSubmit ) {
            formTitle  = 'Edit contact';
            submitName = 'Update contact';
        }
        else {
            formTitle = 'Contact detail'
        }

        const inputClass = this.props.handleSubmit
            ? 'form-control'
            : 'form-control-plaintext';

        const readOnly = this.props.handleSubmit
            ? ''
            : 'readOnly';

        return (
            <form onSubmit={this.handleSubmitLocal}>
                { this.props.contact_id &&
                    <input type="hidden" name="contact_id" value={this.props.contact_id} />
                }
                { this.state.contact_email_id &&
                    <input type="hidden" name="contact_email_id" value={this.state.contact_email_id} />
                }
                { this.state.contact_phone_number_id &&
                    <input type="hidden" name="contact_phone_number_id" value={this.state.contact_phone_number_id} />
                }
                { this.state.contact_address_id &&
                    <input type="hidden" name="contact_address_id" value={this.state.contact_address_id} />
                }

                <h4>{formTitle}</h4>
                <div className="form-group">
                    <div className="form-row">
                        <div className="col">
                            <label htmlFor="firstName">First name</label>
                            <input
                                type="text"
                                id="firstName"
                                value={this.state.first_name}
                                name="first_name"
                                className={inputClass}
                                placeholder="First name"
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                        <div className="col">
                            <label htmlFor="lastName">Last name</label>
                            <input
                                type="text"
                                id="lastName"
                                name="last_name"
                                value={this.state.last_name}
                                className={inputClass}
                                placeholder="Last name"
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                    </div>
                </div>
                <div className="form-group">
                    <div className="form-row">
                        <div className="col">
                            <label htmlFor="email">E-mail</label>
                            <input
                                type="text"
                                id="email"
                                name="email"
                                value={this.state.email}
                                className={inputClass}
                                placeholder="E-mail"
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                        <div className="col">
                            <label htmlFor="phoneNumber">Phone number</label>
                            <input
                                type="text"
                                id="phoneNumber"
                                name="phone_number"
                                value={this.state.phone_number}
                                className={inputClass}
                                placeholder="Phone number"
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="address">Address</label>
                    <input
                        type="text"
                        id="address"
                        name="address"
                        value={this.state.address}
                        className={inputClass}
                        placeholder="1234 Main St"
                        onChange={this.handleInputChange}
                        readOnly={readOnly}
                    />
                </div>
                <div className="form-group">
                    <div className="form-row">
                        <div className="col">
                            <label htmlFor="city">City</label>
                            <input
                                type="text"
                                id="city"
                                name="city"
                                value={this.state.city}
                                className={inputClass}
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                        <div className="col">
                            <label htmlFor="region">Region</label>
                            <input
                                type="text"
                                id="region"
                                name="region"
                                value={this.state.region}
                                className={inputClass}
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                        <div className="col">
                            <label htmlFor="zipcode">Zip</label>
                            <input
                                type="text"
                                id="zipcode"
                                name="zip_code"
                                value={this.state.zip_code}
                                className={inputClass}
                                onChange={this.handleInputChange}
                                readOnly={readOnly}
                            />
                        </div>
                    </div>
                </div>

                <div className="form-row">
                    {this.props.handleSubmit &&
                        <div className="col-xs-1">
                            <button type="submit" className="btn btn-primary">{submitName}</button>
                        </div>
                    }
                    <div className="col-xs-1">
                        <button type="button" className="btn btn-secondary" onClick={this.props.handleCancel}>Cancel</button>
                    </div>
                </div>
            </form>
        );
    }
}

