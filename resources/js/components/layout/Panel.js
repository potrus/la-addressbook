import React from 'react';

class Panel extends React.Component {
    render() {
        const panelClass = this.props.panelClass || 'panel';
        const panelTitle = this.props.title;
        const panelBody  = this.props.body;

        return (
            <div className={panelClass}>
                {panelTitle &&
                    <h2 className="panel-title">{panelTitle}</h2>
                }
                {panelBody}
            </div>
        );
    }
}

export default Panel;
