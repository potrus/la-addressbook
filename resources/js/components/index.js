import React from 'react';
import ReactDOM from 'react-dom';

import AddressBookController from './AddressBookController.js';

ReactDOM.render(
  <AddressBookController />,
  document.getElementById('root')
);
