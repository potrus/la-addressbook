<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Address Book</title>
    <!-- Styles -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{ asset('bootstrap-4.4.1/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font awesome -->
    <link href="{{ asset('fontawesome-5.12.0-web/css/all.min.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="root"></div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('jquery-3.4.1.slim/jquery-3.4.1.slim.min.js') }}"></script>
    <script src="{{ asset('bootstrap-4.4.1/js/bootstrap.min.js') }}"></script>
</body>
</html>
